for i = 1:Nt
   M = getM(Y(i, 9:10), Y(i, 11:16));
   wU = M\[Y(i,1:2)'; Y(i, 3:8)'];
   
   w = wU(1:N);
   w = zeros(N, 1);% для случая несжимаемых пузырей
   U = wU(N+1:end);
   
   Y(i, 17:22) = U;
%    Y(i, 23:28) = 1/( 0 - 4/3*pi*rho_l*Y(i,9:10)) * ()
end

for i = 1:Nt
    Y(i, 23:25) = 1/( 0 - 4/3*pi*rho_l*Y(i,9)^3) * ( Y(i, 3:5) - 2*pi*rho_l*Y(i,9)^3*Y(i, 17:19));
end