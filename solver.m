function dy = solver( t, y )
% solver for ode45 function
global N m
global rho_l P0 Ap omega gamma sigma Pg0 R01

%   y = [ [s1 s2 ... ] [p1___ p2___ ...] [a1 a2 ...] [ r1___ r2___ ...] ];
s = y(1:N);
p = y(N+1:4*N);
a = y(4*N+1:5*N);
r = y(5*N+1:8*N);


p_inf = P0;%P0 - Ap*sin(omega*t);
p_S = zeros(N, 1);
for i = 1:N
    p_S(i) = Pg0*(R01/a(i))^(3*gamma)-2*sigma/a(i);
end

M = getM( a, r );

% [ w U ] from (63)
[wU] = M\[s; p]

w = wU(1:N);
w = zeros(N, 1);% для случая несжимаемых пузырей
U = wU(N+1:end);

% u = A1*p - A2*U;% можно попробовать решить напрямую без использования матриц A1 и A2
u = zeros(3*N, 1);
for j=1:N
    u(3*j-2:3*j) =  1/(m(j)-4/3*pi*rho_l*a(j)^3)*( p(3*j-2:3*j)-2*pi*rho_l*a(j)^3*U(3*j-2:3*j) );
end

ul = u - U; % compare ul obtained from (58) with ul obtained from (61)


dy = zeros(8*N,1);
for j = 1:N
    dy(j) = -1/2*w(j)^2 + 1/4*dot(U(3*j-2:3*j), U(3*j-2:3*j)) - dot(u(3*j-2:3*j),ul(3*j-2:3*j)) + 1/2*dot(ul(3*j-2:3*j), ul(3*j-2:3*j)) + (p_S(j) - p_inf)/rho_l;
    dy(N+3*j-2:N+3*j) = -4*pi*rho_l*a(j)^2*w(j)*ul(3*j-2:3*j);
    dy(4*N+j) = w(j);
    dy(5*N+3*j-2:5*N+3*j) = u(3*j-2:3*j);  
end


end

